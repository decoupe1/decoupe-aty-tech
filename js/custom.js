$(document).ready(function(){
    $("#to-header").on('click', function(event) {
        if (event.isTrigger == undefined) {
            $('html, body').animate({
                scrollTop: $("#wrapper").offset().top - 1500
            }, 1000);
        }
    });
    
    $("#password-show-modal").click(function(){
        $("#sign-in").hide();
        $(".modal-backdrop").hide();
    });
});